const papa = require('papaparse')
const fs = require('fs')
const geolib = require('geolib')

const { prefix, sportrecIds } = require('./constants')

const participants = sportrecIds
  .map(id => JSON.parse(fs.readFileSync(`./sportrec/${prefix}competition_${id}.json`, 'utf8')).participants)
  .flat()
  .map(({ device_id, extrainfo }) => ({ device_id, nr: extrainfo[1].value }))
const tracks = sportrecIds
  .map(id => JSON.parse(fs.readFileSync(`./sportrec/${prefix}history_${id}.json`, 'utf8')).locations)
  .flat()
  .map(({ device_id, locations }) => ({ device_id, locations }))

papa.parse(fs.readFileSync(prefix + 'results.csv', 'utf8'), {
  header: true,
  complete: ({ data }) => {
    data.forEach(({ nr, place }, i) => {
      if (place === '-') { return }
      const deviceId = participants[participants.map(({ nr }) => nr)
        .indexOf(nr)]?.device_id
      if (!deviceId) { return }
      const locations = tracks[tracks.map(({ device_id }) => device_id)
        .indexOf(deviceId)].locations
      const distance = geolib.getPathLength(locations) / 1000
      data[i].distance = distance.toString()
      data[i].pointsPerDistance = (data[i].result / distance).toFixed(2)
    })
    const columns = [...new Set(data.map(team => Object.keys(team)).flat())]
    fs.writeFileSync(prefix + 'results.csv', papa.unparse(data, { columns }))
  }
})