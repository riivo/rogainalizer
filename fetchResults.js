const fetch = require('node-fetch')
const fs = require('fs')

const { prefix, url } = require('./constants')

fetch(url)
  .then(res => res.text())
  .then(res => res.match(/teamresult_\d+\.htm/g)
    .forEach(file =>
      fetch(new URL(url.replace(/results\.htm$/, '') + file))
        .then(res => res.text())
        .then(res =>
          fs.writeFile('./teamresults/' + prefix + '_' + file, res, err => {
            if (err) { return console.error(err) }
            console.log('Downloaded', prefix + '_' + file)
          })
        )
    )
  )