const cheerio = require('cheerio')
const fs = require('fs')
const moment = require('moment')
require('moment-duration-format')
const papa = require('papaparse')

const { prefix } = require('./constants')
const distance = d => d.replace(/.*\/| km/g, '').replace(',', '.') || '0.00'

fs.writeFileSync(prefix + '_results.csv', papa.unparse(
  fs.readdirSync('./teamresults/')
    .filter(file => file.match(prefix))
    .map(file => {
      const $ = cheerio.load(fs.readFileSync('./teamresults/' + file))
      const points = $('td', $('table.r0').eq(1)).eq(3).text()
      const time = moment.duration($('td', $('table.r0').eq(1)).eq(7).text())
      const result = $('td', $('table.r0').eq(1)).eq(9).text()
      const controlsCount = $('[class^=time_ND]').length
      const displacement = distance($('td:contains(km)', $('.resultheader').last()).text())
      const sunsetDisplacement = distance($('td', $('td.time_ND0').parent()).eq(5).text())
      const sunriseDisplacement = distance($('td', $('td.time_ND0').last().parent().next()).eq(5).text())
      return {
        nr: $('table.r0 td').eq(1).text(),
        name: $('table.r0 td').eq(3).text(),
        class: $('td', $('table.r0').first()).last().text(),
        members: $('.css_members').text().trim().replace(/\n/g, ';'),
        place: $('td', $('table.r0').eq(1)).eq(1).text().replace(/\s.*/, ''),
        placeInClass: $('td', $('table.r0').eq(1)).eq(1).text().replace(/\d+/, '').trim(),
        points,
        penalty: $('td', $('table.r0').eq(1)).eq(5).text(),
        time: time.format('d.hh:mm:ss'),
        result,
        controlsCount,
        nightControlsCount: $('[class=time_ND0]').length,
        averageControlValue: (points / controlsCount).toFixed(2),
        displacement,
        sunsetDisplacement,
        sunriseDisplacement,
        nightDisplacement: (sunriseDisplacement - sunsetDisplacement).toFixed(2),
        pointsPerDisplacement: (points / displacement).toFixed(2),
        pointsPerMinute: (points / time.asMinutes()).toFixed(3)
      }
    })
))