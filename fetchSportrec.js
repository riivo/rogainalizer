const fetch = require('node-fetch')
const fs = require('fs')

const { prefix, sportrecIds } = require('./constants')

sportrecIds.forEach(id =>
  ['competition', 'history'].forEach(type =>
    fetch(`https://sportrec.eu/ui/nsport_admin/index.php?r=api/${type}&id=${id}`)
      .then(res => res.text()).then(res => {
        const file = `${type}_${id}.json`
        fs.writeFile('./sportrec/' + prefix + '_' + file, res, err => {
          if (err) { return console.error(err) }
          console.log('Downloaded', prefix + '_' + file)
        })
      })
  )
)